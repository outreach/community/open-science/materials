# Exemples de critères d'adoption de SWH

## France
### Univ. de Lille : LORD
Rapport d'activité 
https://bu.univ-lille.fr/fileadmin/user_upload/SCD/DocumentsSCD/RapportActivite/RA_SCD2023_VersionWeb.pdf 
LORD
Lille Open Research Data
Financé par le Comité National pour La Science Ouverte, porté par l’Université de Lille et inscrit dans sa stratégie Science Ouverte, LORD est un dispositif complet d’assistance et d’accompagnement des équipes de recherche de la métropole lilloise sur l’ensemble des volets concernant la gestion des données, des codes et des logiciels de la recherche. 

#### Connexions possibles avec SWH
- SWH Academy: actions d'accompagnement et production de ressources

### Univ. de Strasbourg
Comité opérationnel
https://factuel.univ-lorraine.fr/node/23778
2023-06-28
"Le comité de pilotage « Science ouverte » a acté le 26 mai 2023 la création d’un «comité opérationnel» chargé d’élaborer, proposer et suivre la mise en œuvre d’une politique d’établissement consacrée à **l’ouverture des codes sources et des logiciels de la recherche**. Il est composé de membres de différents horizons : chercheurs, ingénieurs, documentalistes, informaticiens ou encore spécialistes juridiques et en propriété intellectuelle.

Le comité, dont la première réunion formelle a eu lieu le 16 juin, a d’ores et déjà de nombreux sujets en ligne de mire : recensement des codes déjà ouverts, identification des pratiques et des outils, mise en place de formations, d’événements de sensibilisation etc.

Une adresse mail a été mise en place afin de permettre de répondre à vos questions ou collecter des propositions et des besoins (par exemple vis-à-vis d’outils tels que GitLab, HAL ou Software Heritage) : logiciels-recherche@univ-lorraine.fr

>> Pour plus d’informations, rendez-vous sur le site Science ouverte à l’Université de Lorraine. https://scienceouverte.univ-lorraine.fr/codes-et-logiciels/la-production-de-logiciels-a-lul/"

### ENS Lyon
Feuille de route SO, action 12
"[...] L’ENS de Lyon s’engage à consolider ce soutien ainsi qu’à promouvoir les bonnes pratiques en lien avec l’utilisation de la plateforme Software Heritage, en encourageant le dépôt des logiciels et des codes sources développés par les laboratoires. Elle intègrera le Software Heritage Identifier (SWHID) à sa politique sur les identifiants ouverts"

